package by.example.domain;

import by.example.HibernateConnection;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.Iterator;
import java.util.List;

public class Companies {

    private Company company = new Company();

    public Integer addCompany(String name, int unp, int personal) {

        Session session = HibernateConnection.getFactory();
        Transaction tx = null;
        Integer compID = null;

        try {
            tx = session.beginTransaction();
            company.setName(name);
            company.setUnp(unp);
            company.setPersonal(personal);
           compID = (Integer) session.save(company);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return compID;
    }

    public void listCompanies() {
        Session session = HibernateConnection.getFactory();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List company = session.createQuery("FROM COMPANY").list();

            for (Iterator iterator = company.iterator(); iterator.hasNext(); ) {
                Company companies = (Company) iterator.next();
                System.out.print("Name: " + companies.getName());
                System.out.print("UNP: " + companies.getUnp());
                System.out.println("  Personal: " + companies.getPersonal());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
