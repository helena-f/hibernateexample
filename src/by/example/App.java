package by.example;

import by.example.domain.Companies;

public class App {

    public static void main(String[] args) {
        Companies companies = new Companies();
        companies.addCompany("Zara", 123456789, 200);
        companies.addCompany("Mango", 987654321, 300);
        companies.listCompanies();
    }

}
